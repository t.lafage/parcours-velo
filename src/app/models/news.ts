export class News {

    title: string;
    author: string;
    content: string;
    description: string;
    publishedAt: Date;
    source: string;
    url: string;
    urlToImage: string;
    
}

export class NewsLiteral {

    articles: News[];
    status: string;
    totalResults: number;

}
