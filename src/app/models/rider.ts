import { Model } from './model';

export class Rider extends Model {

    username: string;
    firstname: Date;
    name: number;
    anniversary: Date;
    password: string;
    email: string;
    image: boolean;
    city_ref: boolean;
    parameters_id: boolean;

    constructor( username: string, password: string ) {
        
        super();

        this.username = username;
        this.password = password;
        
    };

}
