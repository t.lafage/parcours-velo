import { Model } from './model';

export class Ride extends Model {

    name: string;
    color: string;
    type: string;
    difficulty: string;
    status: string;
    trace: string;
    rider: number;

    detailIsDisplay?: boolean;

    constructor(
        name: string,
        color: string,
        type: string,
        difficulty: string,
        status: string,
        trace: string,
        riderId: number
    ) {
        super();

        this.name = name;
        this.color = color;
        this.type = type;
        this.difficulty = difficulty;
        this.status = status;
        this.trace = trace;
        this.rider = riderId;
    };
}
