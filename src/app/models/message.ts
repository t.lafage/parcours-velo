export class Message {

    author: string;
    content: string;
    color: string;

    constructor(
        author: string,
        content: string,
        color: string
    ) {
        this.author = author;
        this.content = content;
        this.color = color;
    }

}
