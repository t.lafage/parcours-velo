import { Component, OnInit, Injectable } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { RideService } from '../services/ride.service';
import { RideInfoService } from '../services/ride-info.service';
import { ConfigService } from '../services/config.service';
import { AuthService } from '../services/auth.service';

import { Ride } from '../models/ride';

import { Config } from './../app.config';

import * as L from 'leaflet';
import 'leaflet-routing-machine';
// import 'openrouteservice-js';
import 'leaflet-control-geocoder';
import 'leaflet-search';
import 'leaflet.locatecontrol';

import * as corslite from 'corslite';
import polyline from '@mapbox/polyline';
import * as geolib from 'geolib';
import * as Nominatim from "nominatim-browser";
import { Rider } from '../models/rider';
import * as d3 from 'd3';

// @Injectable()
// export class RouteBackService {
//     public getPreviousUrl(routeArray): string {
//     let prevRoute = '';
//     for (let i = 0; i < routeArray.length - 1; i++) {
//     if (routeArray[i].url._value[0].length > 0) {
//         prevRoute += routeArray[i].url._value[0].path + '/';
//         } 
//         }
//     return prevRoute.slice(0, -1);
//     }
// }

@Component({
    selector: 'app-map',
    templateUrl: './map.page.html',
    styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

    // Eléments relatifs à la carte
    map: L.Map;
    mapClickEvent: L.LeafletEventHandlerFn;
    
    
    // Eléments relatifs aux parcours
    rideViewItinerary: L.Routing.Control;
    currentItinerary: L.Routing.Control;
    rides: Ride[] = [];
    arrayPoints: L.LatLng[];
    
    // Elements graphiques intégrés à la carte
    loginControl: L.Control;
    signinControl: L.Control;
    openRouteService: (new (...args: any[]) => any) & typeof L.Class;
    validBtn: (new (...args: any[]) => any) & typeof L.Class;
    menuBtn: (new (...args: any[]) => any) & typeof L.Class;
    closeRideBtn: (new (...args: any[]) => any) & typeof L.Class;
    DOMtoogle: HTMLCollectionOf<HTMLAnchorElement>;

    // Drapeaux
    isFirstItinerary = true;
    isFirstClickOutSearchBar = true;
    chatIsDisplay = false;
    closeRideMode = false;
    layersSlctIsDisplay = false;

    constructor(
        private rideService: RideService,
        private authservice: AuthService,
        private rideInfoService: RideInfoService,
        private configService: ConfigService,
        private router: Router
    ) { }

    dpLogin(): void {

        if( !this.authservice.user ) {
            console.log('user not connected');

            const loginControl = L.Control.extend({
                options: { position: 'topleft' },
                initialize: options => {
                    L.Util.setOptions(this, options);
                },
                onAdd: map => {
                    var container = L.DomUtil.create('ion-card', 'leaflet-bar leaflet-control leaflet-control-custom');
                    container.style.height = '366px';
                    container.style.width = '281px';
                    container.style.color = 'rgb(119, 119, 119)';
                    container.style.top = '52px';
                    container.style.left = '36px';
                    container.style.backgroundColor = 'rgb(255, 255, 255)';
                    container.style.position = 'fixed';
                    container.style.padding = '10px';
                    container.innerHTML =
                            `<ion-card-header style="font-size: 24px;">Connexion</ion-card-header>
                            <ion-card-content>
                            </ion-card-content>`;

                    var DOMuserInp = <HTMLIonInputElement>L.DomUtil.create('ion-input');
                    DOMuserInp.innerText = "Nom d'utilisateur";
                    container.append(DOMuserInp);

                    var DOMpassInp = <HTMLIonInputElement>L.DomUtil.create('ion-input');
                    DOMpassInp.innerText = "Mot de passe";
                    container.append(DOMpassInp);

                    var DOMloginBtn = L.DomUtil.create('ion-button');
                    DOMloginBtn.innerText = "Se connecter";
                    DOMloginBtn.addEventListener('click', () => this.login(DOMuserInp.value, DOMpassInp.value));
                    container.append(DOMloginBtn);

                    var DOMsigninBtn = L.DomUtil.create('ion-anchor');
                    DOMsigninBtn.innerText = "Créer un compte";
                    DOMsigninBtn.addEventListener('click', () => this.dpSignin());
                    container.append(DOMsigninBtn);

                    L.DomEvent.disableScrollPropagation(container);
                    L.DomEvent.disableClickPropagation(container);

                    return container;
                },
                onRemove: map => { }
            });

            this.loginControl = new loginControl().addTo(this.map);
        }

        else
            this.router.navigateByUrl('/profil');
        
    }

    dpSignin(): void {

        this.map.removeControl(this.loginControl)

        const signinControl = L.Control.extend({
            options: { position: 'topleft' },
            initialize: options => {
                L.Util.setOptions(this, options);
            },
            onAdd: map => {
                var container = L.DomUtil.create('ion-card', 'leaflet-bar leaflet-control leaflet-control-custom');
                container.style.height = '466px';
                container.style.width = '331px';
                container.style.color = 'rgb(119, 119, 119)';
                container.style.top = '32px';
                container.style.left = '16px';
                container.style.backgroundColor = 'rgb(255, 255, 255)';
                container.style.position = 'fixed';
                container.style.padding = '10px';
                container.style.overflow = 'auto';
                container.innerHTML =
                        `<ion-card-header style="font-size: 24px;">Inscription</ion-card-header>
                        <ion-card-content>
                            <ion-item>
                                <ion-label style="font-size: 14px;" position="floating">Nom d'utilisateur</ion-label>
                                <ion-input></ion-input>
                            </ion-item>
                            <ion-item>
                                <ion-label style="font-size: 14px;" position="floating">Prénom</ion-label>
                                <ion-input></ion-input>
                            </ion-item>
                            <ion-item>
                                <ion-label style="font-size: 14px;" position="floating">Nom</ion-label>
                                <ion-input></ion-input>
                            </ion-item>
                            <ion-item>
                                <ion-label style="font-size: 14px;" position="floating">Date</ion-label>
                                <ion-input></ion-input>
                            </ion-item>
                            <ion-item>
                                <ion-label style="font-size: 14px;" position="floating">Mot de passe</ion-label>
                                <ion-input></ion-input>
                            </ion-item>
                            <ion-item>
                                <ion-label style="font-size: 14px;" position="floating">Confirmation du mot de passe</ion-label>
                                <ion-input></ion-input>
                            </ion-item>
                            <ion-item>
                                <ion-label style="font-size: 14px;" position="floating">E-mail</ion-label>
                                <ion-input></ion-input>
                            </ion-item>
                        </ion-card-content>`;

                var DOMsigninBtn = L.DomUtil.create('ion-button');
                DOMsigninBtn.innerText = "S'inscrire";
                DOMsigninBtn.addEventListener('click', () => this.signin());
                container.append(DOMsigninBtn);

                L.DomEvent.disableScrollPropagation(container);
                L.DomEvent.disableClickPropagation(container);

                return container;
            },
            onRemove: map => { }
        });

        this.signinControl = new signinControl().addTo(this.map);
        
    }

    login(username, password): void {

        console.log('login');

        var user = new Rider(
            username,
            password
        )

        this.authservice.auth(user).subscribe();

        this.map.removeControl(this.loginControl)

    }

    signin(): void {

        console.log('signin');

        this.map.removeControl(this.signinControl)
        
    }

    getNearestRideViews(): void {

        if( this.rideViewItinerary ) {
            this.map.removeControl(this.rideViewItinerary);
            this.rideViewItinerary = null;
            return;
        }

        var firstPointArray: L.LatLng[] = []

        this.rideService.all()
            .subscribe(rides => {

                for (let ride of rides)
                    firstPointArray.push(JSON.parse(ride.trace)[0]);
                    
                var nearestTrace = geolib.findNearest(
                    this.configService.userPos,
                    firstPointArray
                );

                for (let ride of rides) {

                    let a = <L.LatLng>nearestTrace;
                    let b = JSON.parse(ride.trace)[0];
                    
                    if (a.lat == b.lat && a.lng == b.lng)
                        this.dpRideViews(ride);

                }

                //TODO: ajouter une propriété zone dans le model.
                //TODO: système qui s'adapte au nombre de trajet dans la zone

            })

    }

    initializeORSJS(): void {

        var openRouteService = L.Class.extend({
            options: {
                serviceUrl: 'https://api.openrouteservice.org/directions',
                timeout: 30 * 1000,
                urlParameters: {}
            },
            initialize: function (apiKey, options) {
                // this._apiKey = apiKey;
                L.Util.setOptions(this, options);
            },
            route: function (waypoints, callback, context, options) {
                var timedOut = false,
                    wps = [],
                    url,
                    timer,
                    wp,
                    i;
    
                options = options || {};
                
                url = this.buildRouteUrl(waypoints, options);
    
                timer = setTimeout(function () {
                    timedOut = true;
                    callback.call(context || callback, {
                        status: -1,
                        message: 'OpenRoueService request timed out.'
                    });
                }, this.options.timeout);
    
                for (i = 0; i < waypoints.length; i++) {
                    wp = waypoints[i];
                    wps.push({
                        latLng: wp.latLng,
                        name: wp.name,
                        options: wp.options
                    });
                }
    
                corslite(url, (err, resp) => {
                    var data;
    
                    clearTimeout(timer);
                    if (!timedOut) {
                        if (!err) {
                            // try {
                            data = JSON.parse(resp.responseText);
                            this._routeDone(data, wps, callback, context);
                        } else {
                            callback.call(context || callback, {
                                status: -1,
                                message: 'HTTP request failed: ' + err
                            });
                        }
                    }
                });
    
                return this;
            },
    
            _routeDone: function (response, inputWaypoints, callback, context) {
                var alts = [],
                    waypoints,
                    waypoint,
                    coordinates,
                    i, j, k,
                    instructions,
                    distance,
                    time,
                    leg,
                    steps,
                    step,
                    maneuver,
                    startingSearchIndex,
                    instruction,
                    path;
    
                context = context || callback;
    
                if (!response.routes) {
                    callback.call(context, {
                        status: response.type,
                        message: response.details
                    });
                    return;
                }
    
                for (i = 0; i < response.routes.length; i++) {
                    path = response.routes[i];
                    coordinates = this._decodePolyline(path.geometry);
                    startingSearchIndex = 0;
                    instructions = [];
                    waypoints = [];
                    time = 0;
                    distance = 0;
    
                    for(j = 0; j < path.segments.length; j++) {
                        leg = path.segments[j];
                        steps = leg.steps;
                        for(k = 0; k < steps.length; k++) {
                            step = steps[k];
                            distance += step.distance;
                            time += step.duration;
                            instruction = this._convertInstructions(step, coordinates);
                            instructions.push(instruction);
                            waypoint = coordinates[path.way_points[1]];
                            waypoints.push(waypoint);
                        }
                    }
    
                    alts.push({
                        name: 'Routing option ' + i,
                        coordinates: coordinates,
                        instructions: instructions,
                        summary: {
                            totalDistance: distance,
                            totalTime: time,
                        },
                        inputWaypoints: inputWaypoints,
                        waypoints: waypoints
                    });
                }
    
                callback.call(context, null, alts);
            },
    
            _decodePolyline: function (geometry) {
                var polylineDefined = polyline.fromGeoJSON(geometry);
                var coords = polyline.decode(polylineDefined, 5),
                    latlngs = new Array(coords.length),
                    i;
                for (i = 0; i < coords.length; i++) {
                    latlngs[i] = new L.LatLng(coords[i][0], coords[i][1]);
                }
    
                return latlngs;
            },
    
            buildRouteUrl: function (waypoints, options) {
                var computeInstructions =
                    true,
                    locs = [],
                    i,
                    baseUrl;
    
                for (i = 0; i < waypoints.length; i++) {
                    locs.push(waypoints[i].latLng.lng + '%2C' + waypoints[i].latLng.lat);
                }
    
                baseUrl = this.options.serviceUrl + '?coordinates=' +
                    locs.join('%7C');
    
                return baseUrl + L.Util.getParamString(L.Util.extend({
                    instructions: true,
                    instructions_format: 'text',
                    geometry_format: 'geojson',
                    preference: 'recommended',
                    units: 'm',
                    profile: 'cycling-regular',
                    api_key: '5b3ce3597851110001cf6248ed970a1d798e4096a3246049a9cb25b8'
                }, this.options.urlParameters), baseUrl);
            },
    
            _convertInstructions: function (step, coordinates) {
                return {
                    text: step.instruction,
                    distance: step.distance,
                    time: step.duration,
                    index: step.way_points[0]
                };
            },
        });

        this.openRouteService = openRouteService;

    }

    dpRideViews(ride: Ride): void {

        // if (this.rideViewItinerary) return;
        
        var data: L.LatLng[] = JSON.parse(ride.trace);

        this.rideViewItinerary = L.Routing.control({
            waypoints: [data[0], data[data.length - 1]],
            routeLine: route => {
                var line = L.Routing.line(route, {
                    styles: [
                        {
                            color: 'black',
                            opacity: 0.15,
                            weight: 9
                        },
                        {
                            color: 'white',
                            opacity: 0.8,
                            weight: 6
                        },
                        {
                            color: 'brown',
                            opacity: 1,
                            weight: 2
                        }
                    ],
                });
                return line;
            },
            show: false,
            router: new this.openRouteService()
        }).addTo(this.map);

    }

    ngOnInit(): void {

        // Resolution provisoire du bug de la reinitialisation de la carte lors de la déconnexion.
        if( this.router.url == '/logout' ) {
            var c = <any>L.DomUtil.get('map');
            c._leaflet_id = null;
        }

        //------début du code-------

        // Initialisation et configuration de la carte
        this.map = L.map('map', Config.mapCnfg).setView([46.8, 2.8], 5);

        // Initialisation des API Open Route Service
        this.initializeORSJS();

        // Fonction de redimensionnement de la carte pour éviter bug d'affichage.
        setTimeout(() => this.map.invalidateSize(), 100);

        // Icone des points d'itinéraire
        L.Marker.prototype.options.icon = L.icon(Config.markerIcon);

        // OPTION: Affiche le parcours instancié dans le service
        // if( this.rideInfoService.rideViews.length ) {
        //     var lastRide = this.rideInfoService.rideViews[this.rideInfoService.rideViews.length - 1];
        //     console.log(lastRide);
            
        //     this.dpRideViews(lastRide);
        // }

        // Chargement/Modification de l'itinéraire au clic sur la carte.
        this.setMapClickEvent();
        this.map.on('click', this.mapClickEvent);

        // Chargement des boutons intégré à la carte
        this.setBtnsCtrl();
        new this.closeRideBtn().addTo(this.map);
        new this.menuBtn().addTo(this.map);

        // Ajout du panneau de sélection du calque de la carte
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(this.map);
        L.control.layers(Config.optionalLayers).addTo(this.map);

        // Barre de recherche de location
        this.setCustomSearchBar();

        // Section provisoire :
        // TODO: retirer les points au clic ( ex: Google Map )
        var testBtn = L.Control.extend({
            options: { position: 'bottomleft' },
            initialize: options => {
                L.Util.setOptions(this, options);
            },
            onAdd: map => {
                var DOM = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
                DOM.style.height = '30px';
                DOM.style.width = '30px';
                DOM.style.backgroundColor = 'white';
                DOM.style.backgroundImage = "url(assets/img/return.png)";
                DOM.style.backgroundSize = "20px 23px";
                DOM.style.backgroundRepeat = "no-repeat";
                DOM.style.backgroundPosition = "3px 1px";
                L.DomEvent.disableClickPropagation(DOM);

                DOM.addEventListener('click', () => {

                    let wpArray = this.currentItinerary.getWaypoints();

                    if( this.closeRideMode && wpArray.length > 3 )
                        this.currentItinerary.spliceWaypoints(wpArray.length - 2, 1);

                    else if( !this.closeRideMode && wpArray.length > 2 )
                        this.currentItinerary.spliceWaypoints(wpArray.length - 1, 1);

                })


                return DOM;
            }
        })
        new testBtn().addTo(this.map);

        // Ajustement CSS sur le sélecteur de calque.
        let DOMlayerContent = <HTMLElement>document.getElementsByClassName('leaflet-control-layers-list')[0];
        let DOMtoogle = <HTMLAnchorElement>document.getElementsByClassName("leaflet-control-layers-toggle")[0];

        DOMtoogle.addEventListener('click', () => {
            DOMtoogle.style.transform = "translate(-6px, -5px)";

            if(this.layersSlctIsDisplay) {
                DOMlayerContent.style.display = "none";
                this.layersSlctIsDisplay = false;
            }
            else {
                DOMlayerContent.style.display = "block";
                this.layersSlctIsDisplay = true;
            }
        })
        
    }

    setBtnsCtrl(): void {

        // Bouton "se géolocaliser"
        L.control.locate(Config.geolocBtn).addTo(this.map);

        // Bouton de validalition du trajet
        this.validBtn = L.Control.extend({
            options: { position: 'topleft' },
            initialize: options => {
                L.Util.setOptions(this, options);
            },
            onAdd: map => {
                var container = L.DomUtil.create('ion-button', 'leaflet-bar leaflet-control leaflet-control-custom');
                container.style.height = '40px';
                container.style.width = '300px';
                container.style.color = 'rgb(119, 119, 119)';
                container.style.bottom = '60px';
                container.style.left = '50px';
                container.style.backgroundColor = 'rgb(255, 255, 255)';
                container.style.position = 'fixed';
                container.innerHTML = '<h2>Valider</h2>';

                container.addEventListener('click', () => {
                    this.router.navigateByUrl('/save-ride');
                })

                L.DomEvent.disableClickPropagation(container);

                return container;
            },
            onRemove: map => {

            }
        });

        // Bouton d'ouverture du menu lateral
        this.menuBtn = L.Control.extend({
            options: { position: 'topleft' },
            onAdd: map => {
                var container = L.DomUtil.create('ion-menu-button', 'leaflet-bar leaflet-control leaflet-control-custom');
                container.style.backgroundColor = 'white';
                // container.style.width = '34px';
                // container.style.height = '34px';
                L.DomEvent.disableClickPropagation(container);

                return container;
            }
        });

        // Bouton de fermeture du trajet
        this.closeRideBtn = L.Control.extend({
            options: { position: 'bottomright' },
            onAdd: map => {
                var DOM = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
                DOM.style.backgroundColor = 'white';
                DOM.style.width = '30px';
                DOM.style.height = '30px';
                DOM.style.backgroundImage = "url(assets/img/close.png)";
                DOM.style.backgroundSize = "21.5px 21.5px";
                DOM.style.backgroundRepeat = "no-repeat";
                DOM.style.backgroundPosition = "2px 2px";
                L.DomEvent.disableClickPropagation(DOM);

                DOM.addEventListener('click', e => {
                    if(!this.currentItinerary) return;

                    var firstWp = this.currentItinerary.getWaypoints()[0];
                    var lastWp = this.currentItinerary.getWaypoints()[this.currentItinerary.getWaypoints().length - 1];
                    
                    if(!this.closeRideMode) {
                        this.currentItinerary.spliceWaypoints(this.currentItinerary.getWaypoints().length, 0, firstWp);
                        this.closeRideMode = true;
                    }

                    else {
                        this.currentItinerary.spliceWaypoints(this.currentItinerary.getWaypoints().length - 1, 1);
                        this.closeRideMode = false;
                    }

                    console.log(firstWp);
                    console.log(lastWp);
                    
                })

                return DOM;
            }
        });

    }

    setMapClickEvent(): void {

        this.mapClickEvent = (e: L.LeafletMouseEvent) => {

            if (this.configService.userPos) {
                if (this.isFirstItinerary) {

                    this.currentItinerary = L.Routing.control({
                        waypoints: [this.configService.userPos, e.latlng],
                        routeLine: route => {
                            var line = L.Routing.line(route, {
                                addWaypoints: true,
                                styles: [
                                    {
                                        color: 'black',
                                        opacity: 0.15,
                                        weight: 9
                                    },
                                    {
                                        color: 'white',
                                        opacity: 0.8,
                                        weight: 6
                                    },
                                    {
                                        color: 'green',
                                        opacity: 1,
                                        weight: 2
                                    }
                                ],
                            });
                            return line;
                        },
                        routeWhileDragging: false,
                        show: false,
                        summaryTemplate: '<h2>{distance}, {time}</h2>',
                        router: new this.openRouteService()
                    }).addTo(this.map);

                    new this.validBtn().addTo(this.map);

                    this.isFirstItinerary = false;

                    this.currentItinerary.on('routeselected', e => {
                        this.arrayPoints = e.route.coordinates;
                        this.rideInfoService.setAltGraphData(this.arrayPoints);
                    });
    
                }
    
                else {

                    let wpArray = this.currentItinerary.getWaypoints()
                    let newWp = <L.Routing.Waypoint><unknown>e.latlng;

                    if( this.closeRideMode )
                        this.currentItinerary.spliceWaypoints(wpArray.length - 1, 0, newWp);

                    else
                        this.currentItinerary.spliceWaypoints(wpArray.length, 0, newWp);
            
                }
            }

        };
    }

    setCustomSearchBar(): void {

        const search = options => new L.Control.Search(options);
        search(Config.SearchBarOpts).addTo(this.map);

        var DOMSearchBar = document.getElementById('searchtext21');
        DOMSearchBar.addEventListener('blur',() => {
            console.log('carte blur');

            this.isFirstClickOutSearchBar = true;

            this.map.off('click', this.mapClickEvent);

            this.map.on('click', () => {
                if( this.isFirstClickOutSearchBar ) {
                    this.isFirstClickOutSearchBar = false;
                }
                else
                    this.map.on('click', this.mapClickEvent);
            });
        })

    }

}
