import * as L from 'leaflet';

export class Config { 
    public static emailTo = 'sanny@gmail.com'; 
    public static KeyVersion = 1.2; 

    public static mapCnfg =
        {
            zoomControl: false
        }

    public static optionalLayers =
        {
            "default": L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png'),
            "WMMap": L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png'),
            "OCMMap": L.tileLayer('http://tile.thunderforest.com/cycle/{z}/{x}/{y}.png'),
            "paysage": L.tileLayer('http://tile.thunderforest.com/landscape/{z}/{x}/{y}.png'),
            "relief": L.tileLayer('http://c.tiles.wmflabs.org/hillshading/{z}/{x}/{y}.png')
        };

    public static geolocBtn: L.Control.LocateOptions =
        {
            strings: {title: "Show me where I am, yo!"},
            drawCircle: false,
            icon: 'map',
            position: 'bottomright'
        }

    public static SearchBarOpts =
        {
            url: 'https://nominatim.openstreetmap.org/search?format=json&q={s}',
            jsonpParam: 'json_callback',
            propertyName: 'display_name',
            propertyLoc: ['lat', 'lon'],
            // container: 'search-bar',
            buildTip: text => `<a href="#" style="background: #fff; width: 100%">${text}</a>`,
            // container: 'search-bar-container',
            textPlaceholder: 'Chercher une location',
            collapsed: false,
            minLength: 2,
            tooltipLimit: 3,
            delayType: 100,
            zoom: 12
        }

    public static markerIcon: L.IconOptions =
        {
            iconSize: [40, 40],
            iconAnchor: [20, 40],
            iconUrl: 'assets/icon/pin.png'
        }
}

// export class App.Config {
// }
