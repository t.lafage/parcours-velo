import { Injectable } from '@angular/core';
import { Events } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { ConfigService } from './config.service';

import { Rider } from '../models/rider';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

    public user: Rider;
    public event: Events

    private serviceUrl: string = `http://${this.configService.serverId}:3000/auth`;

    constructor(
        private http: HttpClient,
        private router: Router,
        public events: Events,
        private configService: ConfigService
    ) { }

    public auth(user: Rider): Observable<Rider> {

        var body = JSON.stringify({
            username: user.username,
            password: user.password
        });

        console.log(this.serviceUrl);
        console.log(body);
        
        return this.http.post<Rider>(
            this.serviceUrl,
            JSON.stringify({
                username: user.username,
                password: user.password
            }),
            { headers: { 'Content-type': 'application/json'} }
        )
            .pipe(
                tap(user => {

                    console.log(user);

                    if (!user.username) return;

                    console.log('connected !');
                    
                    this.user = user;

                    this.events.publish('userLogged',this.user);

                })
            );

    }

    public logout(){

        console.log('logout');
        
        
        this.user = null;
        
    }

}
