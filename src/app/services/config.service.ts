import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import * as L from 'leaflet';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  localIp: string;
  serverId: string;
  userPos: L.LatLng;
  appPages: Array<{title: string, url: string, icon: string}>;

  constructor(private http: HttpClient) { }

  // updateAppPages() {
  //   this.appPages = appPages;

  //   console.log(appPages);
    
  // }

  failGetCurrentPosition() {
    
    var tt = this.http.get<L.LatLng>('https://ipinfo.io/geo')
      .pipe(tap( (res: any) => {
        var loc = res.loc.split(',');
        this.userPos = new L.LatLng(loc[0], loc[1]);
        console.log(`position de l\'utilisateur: ${this.userPos.lat}; ${this.userPos.lng}`)
      }));

    tt.subscribe();

  }

  setLocalIp() {

    function getIPs(callback){
      
        var
          ip_dups = {},
          servers = {iceServers: [{urls: "stun:stun.services.mozilla.com"}]},
          pc = new RTCPeerConnection(servers);

        function handleCandidate(candidate){
          
            var ip_regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/
            var ip_addr = ip_regex.exec(candidate)[1];
            
            if(ip_dups[ip_addr] === undefined)
                callback(ip_addr);
            ip_dups[ip_addr] = true;
        }

        //listen for candidate events
        pc.onicecandidate = ice => {
            if(ice.candidate)
                handleCandidate(ice.candidate.candidate);
        };

        //create a bogus data channel
        pc.createDataChannel("");

        //create an offer sdp
        pc.createOffer()
          .then(result => {
              pc.setLocalDescription(result);
          });

        //wait for a while to let everything done
        setTimeout(() => {
            var lines = pc.localDescription.sdp.split('\n');

            lines.forEach(function(line){
                if(line.indexOf('a=candidate:') === 0)
                    handleCandidate(line);
            });
        }, 1000);

    }
    
    getIPs(ip => {

      // if( location.host ) {

      //   var str = location.host;
      //   this.serverId = str.substring( 0, str.length - 5);

      // }
      
      this.serverId = '192.168.43.38';
      // this.serverId = 'localhost';
      console.log(this.serverId );
      

      this.localIp = ip
      console.log('Adresse ip locale : ' + this.localIp);
      
    });

  }
}
