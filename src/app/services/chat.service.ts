import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { NavController } from '@ionic/angular';
// import * as Observable from 'rxjs/Observable';
import { fromEvent, Observable } from 'rxjs';
import { Message } from '../models/message';
import { ConfigService } from './config.service';

// const socket = io('http://localhost:3000');
// const source = Observable.fromEvent(input, 'click');
// const obs = fromEvent(a, b);

@Injectable({
  providedIn: 'root'
})
export class ChatService {

    private socket: SocketIOClient.Socket;

    constructor(private configService: ConfigService) {
      
      this.socket = io(`http://${this.configService.serverId}:3000`);

    }

    sendChat(message: Message){

        this.socket.emit('chat', message);

    }

    receiveChat(): SocketIOClient.Emitter {

        return this.socket.on('chat', () => {});

    }

    getUsers(): SocketIOClient.Emitter {
        return this.socket.on('users', () => {});
    }

}