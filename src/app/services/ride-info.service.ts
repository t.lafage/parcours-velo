import { Injectable } from '@angular/core';

import * as d3 from 'd3';
import * as L from 'leaflet';
import polyline from '@mapbox/polyline';

import { Ride } from '../models/ride';

interface LatLiteral {

    elevPos: number;
    elevNeg: number;
    totalKm: number;
    data: Array<DataLiteral>
    lastLatLng: L.LatLng;

}

interface DataLiteral {

    distance: number;
    elevation: number;

}

@Injectable({
  providedIn: 'root'
})
export class RideInfoService {

    altGraphData: L.LatLng[] = [];
    rideViews: Ride[] = [];

    // currentRide: L.LatLng[] = [];
    // currentRide: Ride;
    
    DOMAltGraphic: HTMLDivElement;
    
    isFirstElevationGraphic = true;
    altGraphCntDef = false;
    altGraphDataDef = false;

    constructor() { }

    setAltGraphData(arrayPoints: L.LatLng[]) {

        console.log(arrayPoints);
        
        var a_arrayPoints: Array<L.LatLng[]> = [];

        for (let i = 0; i < 5; i++)
            a_arrayPoints[i] = arrayPoints.slice( 500 * i, 500 * i + 500);

        console.log(a_arrayPoints);

        // this.callCykelbanor(a_arrayPoints);
        this.callORSAPI(a_arrayPoints);

    }

    async callORSAPI(a_arrayPoints: Array<L.LatLng[]>) {

        this.altGraphData = [];

        for (let arrayPoints of a_arrayPoints) {

            let body = JSON.stringify({
                    format_in: "polyline",
                    format_out: "polyline",
                    geometry: arrayPoints.map(c => [c.lng, c.lat])
                });
            
            let res = await fetch(
                'https://api.openrouteservice.org/elevation/line',
                {
                    method: 'POST',
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "5b3ce3597851110001cf6248ed970a1d798e4096a3246049a9cb25b8"
                    },
                    body: body
                }
            );
            let datas = await res.json();

            console.log(datas);

            // let coords = polyline.decode(polylineDefined, 5),
            //     latlngs = new Array(coords.length);

            // for (let i = 0; i < coords.length; i++)
            //     latlngs[i] = new L.LatLng(coords[i][0], coords[i][1]);

            // console.log(latlngs);

            // for (let data of datas.coordinates)
            //     this.altGraphData.push( new L.LatLng(data[1], data[0], data[2]));

            for (let data of datas.geometry)
                this.altGraphData.push( new L.LatLng(data[1], data[0], data[2]));

            console.log(this.altGraphData);

        }
        
        this.altGraphDataDef = true;

        if( this.altGraphCntDef )
            this.updateAltGraphCnt();

    }

    async callCykelbanor(a_arrayPoints: Array<L.LatLng[]>) {

        this.altGraphData = [];

        for (let arrayPoints of a_arrayPoints) {

            console.log(
                JSON.stringify(
                    {
                        type: 'LineString',
                        coordinates: arrayPoints.map(c => [c.lng, c.lat])
                    }
                )
            );
            
            var res = await fetch(
                'https://data.cykelbanor.se/elevation/geojson?access_token=0555109ea6bd0e7611e62d755475f9e7',
                {
                    method: 'POST',
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify(
                        {
                            type: 'LineString',
                            coordinates: arrayPoints.map(c => [c.lng, c.lat])
                        }
                    )
                }
            );
            var datas = await res.json();

            console.log(datas);

            for (let data of datas.coordinates)
                this.altGraphData.push( new L.LatLng(data[0], data[1], data[2]));

            console.log(this.altGraphData);

        }
        
        this.altGraphDataDef = true;

        if( this.altGraphCntDef )
            this.updateAltGraphCnt();

    }

    setDOMAltGraph(DOMAltGraphic) {

        this.DOMAltGraphic = DOMAltGraphic;
        this.altGraphCntDef = true;

    }

    updateAltGraphCnt() {

        console.log('Mis à jour du profil altimétrique.');
        this.drawD3Graphic();

    }

    drawD3Graphic() {

        this.DOMAltGraphic.innerHTML = '';

        let margin = {top: 4, right: 10, bottom: 20, left: 30};

        let width = 320 - margin.left - margin.right;
        let height = 120 - margin.top - margin.bottom;

        let x = d3.scaleLinear().range([0, width]);
        let y = d3.scaleLinear().range([height, 0]);

        let xAxis = d3.axisBottom(x).ticks(5);
        let yAxis = d3.axisLeft(y).ticks(5);

        let data = this.altGraphData
            .reduce((lat: LatLiteral, alty) => {            
                if (lat.lastLatLng) {
                    let dist = lat.lastLatLng.distanceTo(alty);
                    lat.totalKm += dist;
                    let elevDiff = alty.alt - lat.lastLatLng.alt;

                    if (elevDiff > 0)
                        lat.elevPos += elevDiff;

                    else
                        lat.elevNeg -= elevDiff;
                }
    
                lat.data.push({
                    distance: lat.totalKm/1000,
                    elevation: alty.alt
                });

                lat.lastLatLng = alty;

                return lat;
            },
            {
                totalKm: 0,
                elevPos: 0,
                elevNeg: 0,
                data: []
            }
        );

        console.log(data);
            
        let statsElem = document.createElement('div');

        statsElem.innerHTML =
            L.Util.template('&#9650;{climb} m, &#9660;{desc} m', {
                climb: Math.round(data.elevPos),
                desc: Math.round(data.elevNeg),
            });
            
        // Scale the range of the data
        x.domain([0, d3.max(data.data, (data: DataLiteral) => data.distance)]);
        y.domain([0, Math.ceil(d3.max(data.data, (data: DataLiteral) => data.elevation ) / 50) * 50]);
        
        // Construction du SVG
        let svg = d3.select(this.DOMAltGraphic)
            .append('svg')
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
            .append('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

        let valueline = d3.line()
            .x((d: any) => x(d.distance))
            .y((d: any) => y(d.elevation));

        // Add the valueline path.
        svg.append('path')
            .attr('class', 'line')
            .attr('d', valueline(data.data));
        
        // Add the X Axis
        svg.append('g')
            .attr('class', 'x axis')
            .attr('transform', 'translate(0,' + height + ')')
            .call(xAxis);
        
        // Add the Y Axis
        svg.append('g')
            .attr('class', 'y axis')
            .call(yAxis);

        this.DOMAltGraphic.append(statsElem);

    }

}
