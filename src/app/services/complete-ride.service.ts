import { Injectable } from '@angular/core';
import { Ride } from '../models/ride';

@Injectable({
  providedIn: 'root'
})
export class CompleteRideService {

  public ride: Ride = null;

  constructor() { }
}
