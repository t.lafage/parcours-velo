import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { News, NewsLiteral } from '../models/news';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  private serviceUrl: string = `https://newsapi.org/v2/everything?apiKey=77306703a13c41aa8286eb341b99f451&q=cycling`;

  constructor(private http: HttpClient) { }

  getNews(): Observable<NewsLiteral> {

    return this.http.get<NewsLiteral>(this.serviceUrl);

  }
  
}
