import { Injectable } from '@angular/core';
import { Rider } from '../models/rider';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class RiderService {

  public riders: Rider[] = [];

  private serviceUrl: string = `http://${this.configService.serverId}:3000/riders`;

  constructor(
      private http: HttpClient,
      private router: Router,
      private configService: ConfigService
  ) { }

    public all(): Observable<Rider[]> {

        return this.http.get<Rider[]>(this.serviceUrl)
            .pipe(tap(riders => this.riders = riders));

    }

    public one(riderId): Observable<Rider> {

        return this.http.get<Rider>(`${this.serviceUrl}/${riderId}`);

    }

    public create(rider): Observable<Rider> {

        return this.http.post<Rider>(
            `${this.serviceUrl}/${rider.id}`,
            JSON.stringify(rider),
            {headers: { 'Content-type': 'application/json'}}
        )
            .pipe(tap(rider => this.riders.push(rider)));

    }

    public update(rider): Observable<Rider> {

        return this.http.put<Rider>(
            `${this.serviceUrl}/${rider.id}`,
            JSON.stringify(rider),
            {headers: { 'Content-type': 'application/json'}}
        )
            .pipe(tap(rider => console.log(rider)));

    }

    public delete(riderId): Observable<Rider[]> {

        return this.http.delete<Rider[]>(`${this.serviceUrl}/${riderId}`);

    }
}
