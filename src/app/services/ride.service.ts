import { Injectable } from '@angular/core';
import { Ride } from '../models/ride';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class RideService {

    public rides: Ride[] = [];

    private serviceUrl: string = `http://${this.configService.serverId}:3000/rides`;

    constructor(
        private http: HttpClient,
        private router: Router,
        private configService: ConfigService
    ) { }

    public all(): Observable<Ride[]> {

        return this.http.get<Ride[]>(this.serviceUrl)
            .pipe(tap(rides => this.rides = rides));

    }

    public allForAnId(userId): Observable<Ride[]> {

        return this.http.get<Ride[]>(`${this.serviceUrl}/user/${userId}`)
            .pipe(tap(rides => this.rides = rides));

    }

    public one(rideId): Observable<Ride> {

        return this.http.get<Ride>(`${this.serviceUrl}/${rideId}`);

    }

    public create(ride): Observable<Ride> {

        console.log(JSON.stringify(ride));
        

        return this.http.post<Ride>(
            this.serviceUrl,
            JSON.stringify(ride),
            {headers: { 'Content-type': 'application/json'}}
        )
            .pipe(tap(ride => this.rides.push(ride)));

    }

    public update(ride): Observable<Ride> {

        return this.http.put<Ride>(
            `${this.serviceUrl}/${ride.id}`,
            JSON.stringify(ride),
            {headers: { 'Content-type': 'application/json'}}
        )
            .pipe(tap(ride => console.log(ride)));

    }

    public delete(rideId): Observable<Ride[]> {

        return this.http.delete<Ride[]>(`${this.serviceUrl}/${rideId}`);

    }
}
