import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'map', pathMatch: 'full'},
  { path: 'map', loadChildren: './map/map.module#MapPageModule'},
  { path: 'logout', loadChildren: './map/map.module#MapPageModule'},
  { path: 'profil', loadChildren: './pages/profil/profil.module#ProfilPageModule' },
  { path: 'stats', loadChildren: './pages/stats/stats.module#StatsPageModule' },
  { path: 'params', loadChildren: './pages/params/params.module#ParamsPageModule' },
  { path: 'explore', loadChildren: './pages/explore/explore.module#ExplorePageModule' },
  { path: 'my-rides', loadChildren: './pages/explore/explore.module#ExplorePageModule' },
  { path: 'trajects', loadChildren: './pages/trajects/trajects.module#TrajectsPageModule' },
  { path: 'annexes', loadChildren: './pages/annexes/annexes.module#AnnexesPageModule' },
  { path: 'save-ride', loadChildren: './pages/save-ride/save-ride.module#SaveRidePageModule' },
  { path: 'pages', loadChildren: './pages/pages.module#PagesPageModule' },
  { path: 'slct-opts', loadChildren: './pages/slct-opts/slct-opts.module#SlctOptsPageModule' },
  { path: 'news', loadChildren: './pages/news/news.module#NewsPageModule' },
  { path: 'try-logout', loadChildren: './utils/page-redirect/page-redirect.module#PageRedirectPageModule'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
