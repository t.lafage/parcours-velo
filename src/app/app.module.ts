import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
// import { EmojiPickerModule } from '@ionic-tools/emoji-picker';
import { PickerModule } from '@ctrl/ngx-emoji-mart'

import { AppRoutingModule } from './app-routing.module';
import { ComponentsModule } from './components/components.module';
import { RideDetailsComponent } from './components/ride-details/ride-details.component';

import { AppComponent } from './app.component';

// import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
// import { Observable } from 'rxjs-compat/Observable';

// const config: SocketIoConfig = { url: 'http://localhost:3001', options: {} };
// import 'ion-floating-menu';
// import io from 'socket.io-client';

// const config: SocketIoConfig = { url: 'http://localhost:3000', options: {}};
// const socket = io('http://localhost:3000');

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    ComponentsModule,
    PickerModule
    // EmojiPickerModule.forRoot(),
    // SocketIoModule.forRoot(config)
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
