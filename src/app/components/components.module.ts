import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { ColorPickerModule } from 'ngx-color-picker';

import { RideDetailsComponent } from './ride-details/ride-details.component';
import { ConnectionComponent } from './utils/connection/connection.component';
import { ChatComponent } from './chat/chat.component';
import { SmileyPickerComponent } from './smiley-picker/smiley-picker.component';

@NgModule({
  imports: [
    IonicModule,
    FormsModule,
    CommonModule,
    PickerModule,
    ColorPickerModule
  ],
  declarations: [
    SmileyPickerComponent,
    RideDetailsComponent,
    ConnectionComponent,
    ChatComponent
  ],
  exports: [
    SmileyPickerComponent,
    RideDetailsComponent,
    ConnectionComponent,
    ChatComponent
  ]
})
export class ComponentsModule { }
