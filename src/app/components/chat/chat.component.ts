import { Component, OnInit, AfterViewInit } from '@angular/core';

// import { EmojiPickerModule } from '@ionic-tools/emoji-picker';

import { ChatService } from '../../services/chat.service';
import { AuthService } from '../../services/auth.service';

import { Message } from '../../models/message';

// import io from 'socket.io-client';

// const socket = io('http://localhost:3000');

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {

    smileyPickerIsDisplay = false;
    color: string;

    users: number = 0;
    message = '';
    messages: Message[] = [];

    // emojitext: string;

    constructor(
        private chatService: ChatService,
        private authService: AuthService,
    ){ }
 
    // handleSelection(event) {
    // this.emojitext = this.emojitext + " " + event.char;
    // }

    ngOnInit() {

        this.chatService.receiveChat()
            .on('chat', (message: Message) => {
                var DOM = <HTMLIonContentElement>document.getElementById("msg-content");
                DOM.scrollToBottom(300);
                this.messages.push(message);
            });

        this.chatService.getUsers()
            .on('users', users => this.users = users);

    }

    addChat(){

        var usrname: string =
            this.authService.user ?
            this.authService.user.username :
            usrname = 'invité';

            console.log(this.color);
            

        var mess = new Message(
            usrname,
            this.message,
            this.color
        )

        this.messages.push(mess);
        this.chatService.sendChat(mess);

        this.message = '';

    }

}
