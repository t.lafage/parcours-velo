import { Component, OnInit, Input } from '@angular/core';
import { Ride } from '../../models/ride';

@Component({
  selector: 'app-ride-details',
  templateUrl: './ride-details.component.html',
  styleUrls: ['./ride-details.component.scss'],
})
export class RideDetailsComponent implements OnInit {

  @Input() ride: Ride

  constructor() { }

  ngOnInit() {}

}
