import { Component, OnInit } from '@angular/core';
import { NewsService } from '../../services/news.service';
import { News } from '../../models/news';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {

    a_news: News[] = [];

    constructor(private newsservice: NewsService) { }

    ngOnInit() {

        this.newsservice.getNews()
            .subscribe( res => {
                console.log(res);
                this.a_news = res.articles;
                console.log(this.a_news);
                
            });

    }

}
