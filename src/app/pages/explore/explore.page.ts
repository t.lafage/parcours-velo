import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { RideService } from '../../services/ride.service';
import { AuthService } from '../../services/auth.service';
import { RideInfoService } from '../../services/ride-info.service';

import { Ride } from '../../models/ride';

@Component({
  selector: 'app-explore',
  templateUrl: './explore.page.html',
  styleUrls: ['./explore.page.scss'],
})
export class ExplorePage implements OnInit {

    rides: Ride[]

    constructor(
        private rideService: RideService,
        private rideInfoService: RideInfoService,
        private router: Router,
        private authService: AuthService
    ) { }

    dpDetails(ride: Ride) {
        console.log(ride);

        if(ride.detailIsDisplay)
            ride.detailIsDisplay = false;

        else
            ride.detailIsDisplay = true;
    }

    setRideView(ride) {

        this.rideInfoService.rideViews.push(ride);

        this.router.navigateByUrl('/map');

    }

    ngOnInit() {

        if( this.router.url == '/my-rides') {
            if( this.authService.user )
                this.rideService.allForAnId(this.authService.user.id)
                .subscribe(rides => this.rides = rides);
            }

        else if( this.router.url == '/explore')
            this.rideService.all()
                .subscribe(rides => this.rides = rides);

    }

}
