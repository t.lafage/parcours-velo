import { Component, OnInit } from '@angular/core';
import { RideService } from '../../services/ride.service';
import { Ride } from '../../models/ride';
import { RideInfoService } from '../../services/ride-info.service';

@Component({
  selector: 'app-slct-opts',
  templateUrl: './slct-opts.page.html',
  styleUrls: ['./slct-opts.page.scss'],
})
export class SlctOptsPage implements OnInit {

    ride: Ride;
    
    name: string;
    color: string;
    type: string;
    difficulty: string;
    status: string;
    trace: Array<L.LatLng>;
    riderId: number;

    saveAuthorized = false;

    constructor(
        private rideService: RideService,
        private rideInfoService: RideInfoService
    ) { }

    ngOnInit() {

        this.trace = this.rideInfoService.altGraphData;
        this.riderId = 1;
        this.name = 'Sans nom';

    }

    saveRide() {

        
        
        this.ride = new Ride(
            this.name,
            this.color,
            this.type,
            this.difficulty,
            this.status,
            JSON.stringify(this.trace),
            this.riderId
        )
        
        this.rideService.create(this.ride).subscribe();

    }

}
