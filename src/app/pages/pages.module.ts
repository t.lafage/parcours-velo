import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagesPage } from './pages.page';
// import { EmojiPickerModule } from '@ionic-tools/emoji-picker';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    // EmojiPickerModule.forRoot(),
    RouterModule.forChild([
      {
        path: '',
        component: PagesPage
      }
    ])
  ],
  declarations: [PagesPage]
})
export class PagesPageModule {}
