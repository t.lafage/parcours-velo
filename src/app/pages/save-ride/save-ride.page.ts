import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';

import { CompleteRideService } from '../../services/complete-ride.service';
import { RideInfoService } from '../../services/ride-info.service';
import { MapService } from '../../services/map.service';

@Component({
    selector: 'app-save-ride',
    templateUrl: './save-ride.page.html',
    styleUrls: ['./save-ride.page.scss'],
})
export class SaveRidePage implements OnInit {

    rideIsReady = false;

    @ViewChild('altGraphic') DOMAltGraphic: ElementRef<HTMLDivElement>;

    constructor(
        private completeRideService: CompleteRideService,
        private rideInfoService: RideInfoService,
        private mapService: MapService,
        private cd: ChangeDetectorRef
    ) { }

    ngOnInit() {

        this.rideInfoService.setDOMAltGraph(this.DOMAltGraphic.nativeElement);

        if(this.rideInfoService.altGraphDataDef) {
            this.rideInfoService.updateAltGraphCnt();
            this.rideIsReady = true;
            console.log(this.rideIsReady);
        }

        else {
            let interval = setInterval(() => {
                if(this.rideInfoService.altGraphDataDef) {
                    this.rideIsReady = true;
                    clearInterval(interval);
                    clearTimeout(timeout);
                    console.log('Le parcours est prêt à être enregistré.');
                }
            }, 500);
    
            let timeout = setTimeout(() => {
                console.log('La connexion est trop lente.');
                clearInterval(interval);
            }, 50000);
        }
    
    }

}
