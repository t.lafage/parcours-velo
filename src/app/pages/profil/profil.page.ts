import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, RoutesRecognized } from '@angular/router';
import { filter, pairwise } from 'rxjs/operators';
import { Location } from '@angular/common';
import { Rider } from '../../models/rider';
import { RiderService } from '../../services/rider.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.page.html',
  styleUrls: ['./profil.page.scss'],
})
export class ProfilPage implements OnInit {

  private previousUrl: string;
  private currentUrl: string;

  user: Rider;

  constructor(
    private router: Router,
    private location: Location,
    private riderService: RiderService,
    private authService: AuthService
  ) {
    // this.router.events.pipe(
    //   filter(e => e instanceof RoutesRecognized),
    //   pairwise()
    // )
    // .subscribe((event: any[]) => {
    //   console.log(event[0].urlAfterRedirects);
    // });
  }

  goBackLocation() {
    this.location.back();
  }

  ngOnInit() {
    // this.router.events
    //     .pipe(
    //       filter(event => event instanceof RoutesRecognized),
    //       pairwise()
    //     )            
    //     .subscribe((e: any) => {
    //         this.previousUrl = e[0].urlAfterRedirects;
    //     });

    // console.log(this.location.back())

    console.log(this.authService.user);
    
    if( !this.authService.user )
      this.riderService.one(1)
          .subscribe(rider => {
            this.authService.user = rider;
            this.user = this.authService.user;
            console.log(this.user);
          });

    this.user = this.authService.user;
    

  }

}
