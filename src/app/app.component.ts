import { Component } from '@angular/core';

import { Platform, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ConfigService } from './services/config.service';

import * as L from 'leaflet';
import { AuthService } from './services/auth.service';

const appPublicsPages = [
    { title: 'Affichage de la carte', url: '/map', icon: 'map' },
    { title: 'Mes trajets', url: '/my-rides', icon: 'list' },
    { title: 'Se connecter', url: '/map', icon: 'contact' },
    { title: 'Explorer les trajets publics', url: '/explore', icon: 'planet' },
    { title: 'Paramètres', url: '/list', icon: 'list' },
    { title: 'A propos', url: '/list', icon: 'list' },
    { title: 'CGU', url: '/try-logout', icon: 'list' },
    { title: 'Réglement du chat', url: '/list', icon: 'list' },
    { title: 'Reporter un commentaire', url: '/list', icon: 'list' },
    { title: 'Activer le tutoriel', url: '/list', icon: 'list' }
];

const appPrivetsPages = [
    { title: 'Affichage de la carte', url: '/map', icon: 'map' },
    { title: 'Mes trajets', url: '/my-rides', icon: 'list' },
    { title: 'Mon profil', url: '/profil', icon: 'contact' },
    { title: 'Explorer les trajets publics', url: '/explore', icon: 'planet' },
    { title: 'Mes statistiques', url: '/list', icon: 'list' },
    { title: 'Paramètres', url: '/list', icon: 'list' },
    { title: 'A propos', url: '/list', icon: 'list' },
    { title: 'Se déconnecter', url: '/try-logout', icon: 'list' },
    { title: 'CGU', url: '/list', icon: 'list' },
    { title: 'Réglement du chat', url: '/list', icon: 'list' },
    { title: 'Reporter un commentaire', url: '/list', icon: 'list' },
    { title: 'Activer le tutoriel', url: '/list', icon: 'list' }
];

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

    appPages = appPublicsPages;

    // event: Events;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private configService: ConfigService,
        public events: Events
    ) {
        this.initializeApp();
    }

    initializeApp() {
        
        this.events.subscribe('userLogged',() => this.appPages = appPrivetsPages);
        this.events.subscribe('userLogout',() => this.appPages = appPublicsPages);

        this.platform.ready().then(() => {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
        });

        this.statusBar.backgroundColorByHexString('#989aa2');
        this.statusBar.styleLightContent();

        this.configService.setLocalIp();

        // this.configService.updateAppPages();

        navigator.geolocation.getCurrentPosition(
            pos => {
                this.configService.userPos = new L.LatLng(pos.coords.latitude, pos.coords.longitude)
                console.log(`Position de l\'utilisateur : ${this.configService.userPos.lat}; ${this.configService.userPos.lng}`);
            },
            error => this.configService.failGetCurrentPosition()
        );

    }

    // actualizarAppPages() {

    //     if ( this.authservice.user )
    //         this.appPages =
        
    // }

}
