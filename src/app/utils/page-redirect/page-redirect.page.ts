import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-page-redirect',
  templateUrl: './page-redirect.page.html',
  styleUrls: ['./page-redirect.page.scss'],
})
export class PageRedirectPage implements OnInit {

  constructor(
    private router: Router,
    private authService: AuthService,
    ) { }

  ngOnInit() {

    console.log(this.router.url);
    
    if( this.router.url === '/try-logout' ) {

      this.authService.logout();

      setTimeout(() => {
        this.router.navigateByUrl('/logout')
      }, 1000);

    }
    
  }

}
